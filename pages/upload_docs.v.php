<?php
    $pid = $_GET['pid'];

    if(isset($_POST['upload_main_docs'])){
        upload_docs($pid, $_POST, $_FILES);

        redirect("/?p=project&pid=$pid");
    }

?>
<div class="section reg-main-block">
    <div class="contaner">
        <div class="row align-items-center">
            <div class="col-xl-4 offset-4">
                <h2>ТЗ</h2>
                <div class="form-registration">
                    <form action = "?p=upload_docs&pid=<?php echo $pid?>" enctype="multipart/form-data" method = "post">
                        <p>ТЗ:</p>
                        <label>
                            <input type = "file" name = "tz">
                        </label>
                        <p>Название ТЗ:</p>
                        <label>
                            <input type = "text" name = "tz_name">
                        </label>

                        <p>Презентация:</p>
                        <label>
                            <input type = "file" name = "preza">
                        </label>

                        <p>Название презентации:</p>
                        <label>
                            <input type = "text" name = "preza_name">
                        </label>

                        <input type = "submit" name = "upload_main_docs" value = "Загрузить">

                    </form>
                </div>
                <!-- /.form-registration -->

            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.contaner -->
</div>