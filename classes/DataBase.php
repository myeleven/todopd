<?php
    Class DataBase {
        public $db;
        private $prep = [];
        public function __construct()
        {
            $db_params = include_once(CONF . SEP .'db_params.php');
            $this->db = new PDO( 'mysql:host=' . $db_params['DB_HOST'] .';dbname=' . $db_params['DB_NAME'], $db_params['DB_USER'], $db_params['DB_PASS']);
            $this->db->query('SET NAMES utf8');
        }

        public function query($query){
            $q = $this->db->query($query);
            $return = $q ? $q->fetchAll(PDO::FETCH_ASSOC) : null;
            return $return != null ? $return : null;
        }

        public function db_prep($query_name, $query) {
            $this->prep[$query_name] = $query;
        }

        public function db_exec($query_name, $args = []) {
            $query = isset($this->prep[$query_name]) ? $this->prep[$query_name] : null;

            if($query != null) {
                $stmt = $this->db->prepare($query);
                $fields = array_keys($args);
                $args = array_values($args);

                for($i = 0; $i < count($args);$i++) {
                    $stmt->bindValue(":$fields[$i]", $args[$i]);
                }

                $stmt->execute();

                return $stmt->fetchAll(PDO::FETCH_ASSOC);
            } else {
                return new Exception('No such prepared query.');
            }
        }

        public function lastId($table, $order = null) {
            $order = $order == null ? 'id' : $order;
            return $this->query("SELECT $order FROM $table ORDER BY $order DESC LIMIT 1")[0][$order];
        }

    }