<section class="main-sect">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-xl-12">
                <h1>TODO <span class="logo">PD</span></h1>
                <p>Сервис для упрощения проверки проектной деятельности кураторам и студентам </p>
                <a href="?p=register&t=0" class="button-custom">Я студент</a>
                <a href="?p=register&t=1" class="button-custom">Я куратор</a>
                <br>
                <br>
                или <a href="?p=auth" class="entry">войти</a>
            </div>
            <!-- /.col-xl-12 -->
            <span class="copy text-dark">#todopd © Москва 2018</span>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container -->
</section>
<!-- /.main-sect -->