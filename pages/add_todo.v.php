<?php

if(isset($_POST['add_todo'])) {
    $project_id = add_todo($_POST, $_FILES);

    var_dump( $project_id);

    if($project_id != 0 && $project_id != null) {
        redirect("/?p=project&pid=$project_id");
    }
}
?>
<div class="row">
    <div class="col-2"></div>
    <div class="col-8 mt-4">
        <div class="add_todo">
            <form action="?p=add_todo" method="post" enctype="multipart/form-data">
                <input hidden name="project_id" value = "<?php echo $_GET['pid']?>">

                <div class="form-group">
                    <label>Название TODO</label>
                    <input class="form-control" name="name"/>
                </div>

                <div class="form-group">
                    <label>Описание или комментарий</label>
                    <input class="form-control" name="description"/>
                </div>

                <div class="form-group">
                    <label>Дедлайн</label>
                    <input class="form-control" name="deadline" type="date"/>
                </div>

                <div class="form-group">
                    <label>Документ (не обязательно)</label>
                    <input class="form-control" name="document_id" type="file"/>
                </div>

                <input class="btn btn-danger" name="add_todo" type="submit"/>
            </form>
        </div>
    </div>
    <div class="col-2"></div>
</div>