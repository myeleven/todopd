<?php
$pid = isset($_GET['pid']) ? $_GET['pid'] : null;

$project_list = get_projects_list($_SESSION['user']['id']);

$project_data = get_project_data($pid)[0];

$user_data = get_user_data($project_data['student_id'])[0];

$teacher_data = get_user_data($project_data['prepod_id'])[0];

$main_documents = get_main_docs($pid);

$todo_items = get_todo($pid);

?>
<div class="row">
    <?php if (count($project_list)): ?>
        <div class="col-lg-2 border-right">
            <ul class="list-of-name-projects">
                <?php foreach ($project_list as $project): ?>
                    <li><a href="?p=project&pid=<?php echo $project['id'] ?>"><?php echo $project['name'] ?></a></li>
                <?php endforeach; ?>
            </ul>
        </div>
        <div class="col-lg-10 pl-0">
            <?php if ($pid != null): ?>
                <div id="project">
                    <div class="card-header bg-dark text-light">
                        <h1><?php echo $project_data['name'] ?></h1>
                    </div>
                    <div class="card-body">
                        <div class="cart-top mb-5">
                            <div class="float-left">
                                <?php if ($_SESSION['user']['id'] == $user_data['id']): ?>
                                    <a href="?p=edit_project&?pid=<?php echo $pid ?>">Редактировать</a>
                                <?php endif; ?>
                            </div>
                            <div class="float-right">
                                Студент: <strong id="student"><?php echo $user_data['fio'] ?></strong><br>
                                Куратор: <strong id="prepod"><?php echo $teacher_data['fio'] ?></strong>
                            </div>
                        </div>
                        <div class="card-text">
                            <div class="description p-4" id="description">
                                <div class="bg-light p-4">
                                    <?php echo $project_data['descr'] ?>
                                </div>
                            </div>
                            <div class="main-documents row">
                                <div class="col-1">
                                </div>
                                <?php if ($main_documents['tz'] != null && $main_documents['prez'] != null): ?>
                                    <div class="col-5 doc-item">
                                        <div class="bg-light border-info border" id="TZ">
                                            <a href="<?php echo $main_documents['tz']['url'] ?>">
                                                <div class="item-text">
                                                    <p class="font-italic">
                                                        <i class="far fa-file-alt mb-2"></i><br>
                                                        <?php echo $main_documents['tz']['name'];?>
                                                    </p>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-5 doc-item">
                                        <div class="bg-light border-warning border" id="PR">
                                            <a href="<?php echo $main_documents['prez']['url'] ?>">
                                                <div class="item-text">
                                                    <p class="font-italic">
                                                        <i class="fas fa-desktop mb-2"></i><br>
                                                        <?php echo $main_documents['prez']['name'];?>
                                                    </p>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                <?php else: ?>
                                    <?php if ($_SESSION['user']['id'] == $user_data['id']): ?>
                                        <div class="col-5">
                                            <a href="?p=upload_docs&pid=<?php echo $pid ?>">Загрузить документы</a>
                                        </div>
                                    <?php endif; ?>
                                <?php endif; ?>
                                <div class="col-1">
                                </div>
                            </div>
                        </div>


                <div class="todo mt-4">
                    <h4>Дополнительные TODO</h4>

                    <?php if($_SESSION['user']['id'] == $user_data['id']): ?>
                        <a href = "?p=add_todo&pid=<?php echo $pid?>">Добавить TODO</a>
                    <?php endif; ?>
                    <?php if ($todo_items != NULL): ?>

                        <?php foreach($todo_items as $todo): ?>
                        <div class="todo-item p-4">
                            <div class="todo-inner bg-light p-4">
                                <i class="todo-name"><?php echo $todo["name"]?></i>
                                <div class="float-right">
                                    <?php echo $todo['deadline']?>
                                </div>
                                <br>
                                <i class="pl-3"><?php echo $todo['description']?></i>
                                <?php if($todo['doc'] != null):?>
                                <i class="pl-3"><a href = "<?php echo $todo['doc']['url']?>">Загрузить документ</a></i>
                                <?php endif;?>
                                <div class="checkbox float-right">
                                    <?php if ($todo["is_ready"]): ?>
                                        <?php if ($_SESSION['user']['id'] == $user_data['id']): ?>
                                            <input id="is_ready" type="checkbox" class="chk-col-black" checked>
                                            <?php else: ?>
                                            <input id="is_ready" type="checkbox" disabled class="chk-col-black" checked>
                                        <?php endif; ?>
                                        <label for="is_ready"></label>
                                    <?php else: ?>
                                        <div class="checkbox float-right">
                                            <?php if ($_SESSION['user']['id'] == $user_data['id']): ?>
                                                <input id="is_ready" type="checkbox" class="chk-col-black">
                                            <?php endif; ?>
                                            <label for="is_ready"></label>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>

                        <?php endforeach; ?>
                    <?php endif; ?>



                    </div>
                </div>
            <?php else: ?>
                выбери свой проект
            <?php endif; ?>
        </div>
    <?php else: ?>
        Нет проектов у пользователя
    <?php endif; ?>
</div>