<?php
    Class Router {
        public static function load_page($page) {

            $page = isset($page['p']) ? $page['p'] : 'main';
            if(file_exists(PGS . SEP . $page . '.v.php'))
                include_once(PGS . SEP . $page . '.v.php');
            else
                echo 'No such page';
        }
    }