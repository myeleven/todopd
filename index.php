<?php
    session_start();

    require_once('configs/configs.php');

    require_once(CONF . SEP . 'autoloader.php');

    require_once(CONF . SEP . 'website_configs.php');

    require_once(CONF . SEP . 'tools.php');
    ?>
<!doctype html>
<html lang="ru">
    <?php include_once(INC . SEP . 'head.inc.php')?>
<body>
    <?php if($auth) include_once(INC . SEP . 'header.inc.php')?>
    <main>
        <?php Router::load_page($_GET); ?>
    </main>
    <?php if($auth) include_once(INC . SEP . 'footer.inc.php')?>
    <script src="../assets/libs/jquery/dist/jquery.min.js"></script>
</body>
</html>
