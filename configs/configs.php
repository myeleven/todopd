<?php
    define('SEP', '/');
    define('ROOT', dirname(__DIR__));

    define('PGS', ROOT . SEP . 'pages');
    define('CONF', ROOT . SEP . 'configs');
    define('CLS', ROOT . SEP . 'classes');
    define('INC', ROOT . SEP . 'includes');
    define('DOCS', ROOT . SEP . 'docs');

    error_reporting(E_ALL);
    ini_set('display_errors',1);