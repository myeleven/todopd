<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?php echo $web_name?></title>

    <link rel="stylesheet" href="../assets/libs/bootstrap-4.0.0/bootstrap-4.0.0/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/css/style.css">
    <link rel="stylesheet" href="../assets/css/projects.css?=<?php rand(0,100000) ?>"/>
    <link rel="stylesheet" href="../assets/fonts/font-awesome/web-fonts-with-css/css/fontawesome-all.min.css">

</head>