<?php
if(isset($_GET['t'])) {
    switch ($_GET['t']) {
        case '0':
            $type = 0;
            break;
        case '1':
            $type = 1;
            break;
    }
}

if(isset($_POST['register_user'])) {
        user_register($_POST);


        redirect('/?p=auth');
}
?>


<div class="section reg-main-block">
    <div class="contaner">
        <div class="row align-items-center">
            <div class="col-xl-4 offset-4">
                <h2>Регистрация</h2>
                <div class="form-registration">
                    <form action = "?p=<?php echo $page?>" method = "post">
                        <p>ФИО:</p>
                        <label>
                            <input type = "text" name = "fio">
                        </label>
                        <p>Факультет(опционально):</p>
                        <label>
                            <input type = "text" name = "facult">
                        </label>

                        <p>Логин:</p>
                        <label>
                            <input type = "text" name = "login">
                        </label>

                        <p>Пароль:</p>
                        <label>
                            <input type = "password" name = "password">
                        </label>

                        <div class="wrap-button-submit">
                            <input type = "submit" name = "register_user" value="Зарегистрироваться">
                        </div>

                    </form>
                </div>
                <!-- /.form-registration -->

            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.contaner -->
</div>
<!-- /.section reg-main-block -->