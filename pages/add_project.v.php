<?php

    if(isset($_POST['add_project'])) {
        $project_id = add_project($_POST);

        if($project_id != 0 && $project_id != null) {
            redirect("/?p=project&pid=$project_id");
        }
    }

    $teachers = get_users_list(1);

?>


<div class="section reg-main-block add-new-project ">
    <div class="contaner">
        <div class="row align-items-center">
            <div class="col-xl-4 offset-4">
                <h2>Добавить новый проект</h2>
                <div class="form-registration">
                    <form action = "?p=<?php echo $page?>" method = "post">
                        <p>Название проекта</p>
                        <label>
                            <input type = "text" name = "name">
                        </label>
                        <p>Описание проекта:</p>
                        <label>
                            <textarea name = "descr"></textarea>
                        </label>

                        <p>Выберите куратора:</p>
                        <label>
                            <select name = "prepod_id">
                                <?php foreach ($teachers as $teacher):?>
                                <option value = "<?php echo $teacher['id']?>"><?php echo $teacher['fio']?></option>
                                <?php endforeach;?>
                            </select>
                        </label>

                <div class="wrap-button-submit">
                        <input type = "submit" name = "add_project" value = "Добавить проект">
                </div>
                    </form>
                </div>
                <!-- /.form-registration -->

            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.contaner -->
</div>
<!-- /.section reg-main-block -->
