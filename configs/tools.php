<?php
function redirect($path)
{
    header('Location:' . $path);
}


function user_auth($login = '', $pass = '')
{
    global $db;

    $user_data = $db->query("SELECT 
                                          id, 
                                          password, 
                                          type, 
                                          login 
                                        FROM 
                                          users 
                                        WHERE 
                                          login = '$login'")[0];

    if (isset($user_data['id'])) {
        if ($user_data['password'] == $pass) {
            $_SESSION['user']['id'] = $user_data['id'];
            $_SESSION['user']['type'] = $user_data['type'];
            redirect('/?p=project');
        } else {
            echo 'Wrong password';
        }
    } else {
        echo 'No such user';
    }
}

function user_register($data) {

    global $db;

    unset($data['register_user']);

    $keys = implode('`,`', array_keys($data));

    $values = implode("','", $data);

    $return = $db->query("INSERT INTO users (`$keys`) VALUES ('$values')");

    return $return == 1 ? true : false;
}

function get_users_list($user_type){
    global $db;

    return $db->query("SELECT * FROM users WHERE type = '$user_type'");
}

function add_project($data) {

    global $db;

    unset($data['add_project']);

    $data['student_id'] = $_SESSION['user']['id'];

    $keys = implode('`,`', array_keys($data));

    $values = implode("','", $data);

    $db->db->query("INSERT INTO `projects` (`$keys`) VALUES ('$values');");


    $project_id = $db->lastId('projects');

    return $project_id;


}

function get_user_data($uid) {
    global $db;

    $data = $db->query("SELECT * FROM users WHERE id = '$uid'");
    return $data;
}


function get_project_data($uid) {
    global $db;
    $data = $db->query("SELECT * FROM projects WHERE id = '$uid'");
    return $data;
}

function get_main_docs($pid) {
    global $db;

    $docs['tz'] = $db->query("SELECT * FROM documents WHERE project_id = '$pid' AND doc_type = 0")[0];
    $docs['prez'] = $db->query("SELECT * FROM documents WHERE project_id = '$pid' AND doc_type = 1")[0];

    return $docs;
}

function get_projects_list($uid) {
    global $db;

    $type = $_SESSION['user']['type'] == 0 ? 'student_id' : 'prepod_id';

    return $db->query("SELECT * FROM projects WHERE $type = '$uid'");
}


function add_todo($data, $file = null){

    global $db;

    unset($data['add_todo']);

    $keys = implode('`,`', array_keys($data));

    $values = implode("','", $data);

    $pid = $data['project_id'];

    $db->query("INSERT INTO `projects_todo` (`$keys`) VALUES ('$values');");


    if($file != null) {
        $file = $file['document_id'];

        if(!is_dir('docs' . SEP . $pid))
            mkdir('docs' . SEP . $pid);
        if(move_uploaded_file($file['tmp_name'],'docs' . SEP . $pid . SEP . $file['name'])) {
            $path = 'docs' . SEP . $pid . SEP . $file['name'];
            $file_name = $file['name'];
            $db->query("INSERT INTO 
                              documents (`url`,`name`, `project_id`, `doc_type`) 
                           VALUES ('$path', '$file_name','$pid',2)");

            $doc_id = $db->lastId('documents');

            $db->query("UPDATE projects_todo SET document_id = '$doc_id' WHERE id = '$pid'");
        }
    }

    return $data['project_id'];
}

function get_todo($pid)
{
    global $db;
    $data = $db->query("SELECT * FROM projects_todo WHERE project_id = '$pid'");
    if($data['document_id'] != ''){
        $doc_id = $data['document_id'];
        $data['doc'] = $db->query("SELECT * FROM documents WHERE id = '$doc_id'");
    }
    return $data;
}


function upload_docs($pid, $post, $files) {
    global $db;

    $tz = $files['tz'];
    $tz_name = $post['tz_name'];
    $preza = $files['preza'];
    $preza_name = $post['preza_name'];

    $tz_ext = get_ext($tz['name']);
    $preza_ext = get_ext($preza['name']);

    if(!is_dir('docs' . SEP . $pid))
        mkdir('docs' . SEP . $pid);
    if(move_uploaded_file($tz['tmp_name'], 'docs' . SEP . $pid . SEP . 'tz' . '.' . $tz_ext)) {
        $path = 'docs' . SEP . $pid . SEP . 'tz' . '.' . $tz_ext;
        $db->query("INSERT INTO 
                              documents (`url`,`name`, `project_id`, `doc_type`) 
                           VALUES ('$path', '$tz_name','$pid',0)");
        echo '<p>ТЗ загружено</p>';
        $ret['tz'] = true;
    }

    if(move_uploaded_file($preza['tmp_name'], 'docs' . SEP . $pid . SEP . 'preza' . '.' . $preza_ext)) {
        $path = 'docs' .SEP . $pid . SEP . 'preza' . '.' . $preza_ext;
        $db->query("INSERT INTO 
                              documents (`url`,`name`, `project_id`, `doc_type`) 
                           VALUES ('$path', '$preza_name','$pid',1)");
        echo '<p>Преза загружена</p>';
        $ret['preza'] = true;
    }

    return $ret;
}

function get_ext($image_type)
{
    return end(explode('.', $image_type));
}