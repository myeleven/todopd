<?php
    if(isset($_POST['auth_user'])) {
        user_auth($_POST['login'], $_POST['password']);
    }
?>

<section class="auth-section">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-xl-4 offset-4">
                <h2>Вход</h2>

                <div class="form-registration">

                    <form action = "?p=<?php echo $page?>" method = "post">
                        <p>Логин:</p>
                        <label>
                            <input type = "text" name = "login">
                        </label>

                        <p>Пароль:</p>
                        <label>
                            <input type = "password" name = "password">
                        </label>

                        <div class="wrap-button-submit">
                         <input type = "submit" name = "auth_user" value="Войти">
                        </div>

                    </form>
                </div>
            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container -->
</section>

